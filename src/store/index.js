import Vue from "vue";
import Vuex from "vuex";
import VuexPersist from "vuex-persist";

Vue.use(Vuex);

const vuexPersist = new VuexPersist({
  key: "vue_crud",
  storage: localStorage
});

export default new Vuex.Store({
  plugins: [vuexPersist.plugin],
  state: {
    idForPost: 1,
    posts: [],
    idForCategory: 1,
    categories: []
  },
  mutations: {
    // Post mutations
    createPost(state, payload) {
      let post = {
        id: state.idForPost,
        title: payload.title,
        category: payload.category,
        body: payload.body
      };
      state.posts.push(post);
      state.idForPost++;
    },
    updatePost(state, payload) {
      let index = state.posts.findIndex(x => x.id === payload.id);
      state.posts.splice(index, 1, payload);
    },
    deletePost(state, payload) {
      let index = state.posts.findIndex(x => x.id === payload);
      state.posts.splice(index, 1);
    },
    // Category Mutation
    createCategory(state, payload) {
      console.log("im am in createCategory");
      let category = {
        id: state.idForCategory,
        name: payload.name
      };
      state.categories.push(category);
      state.idForCategory++;
    },
    updateCategory(state, payload) {
      let index = state.categories.findIndex(x => x.id === payload.id);
      state.categories.splice(index, 1, payload);
    },
    deleteCategory(state, payload) {
      let index = state.categories.findIndex(x => x.id === payload);
      state.categories.splice(index, 1);
    },
    createCategoryFromPost(state, payload) {
      let category = {
        id: state.idForCategory,
        name: payload.name
      };
      state.categories.push(category);
      state.idForCategory++;
    }
  },
  actions: {},
  modules: {}
});
